﻿using UnityEngine;
using System.Collections;

public class coinButton : MonoBehaviour {

    static public int lives = 0;
    public GameObject ball;
	// Use this for initialization
	void Awake () {
        ball = GameObject.Find("ball");
        ball.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (lives <= 0)
        {
            ball.SetActive(false);
        }
    }
    void OnMouseDown()
    {
        ball.SetActive(true);
        if (lives <= 0)
        {
            GameManager.Score = 0;
            GameManager.triple = 0;
            lives += 3;
            GameManager.start = true;
        }
        
    }
}
