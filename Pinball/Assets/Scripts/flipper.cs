﻿using UnityEngine;
using System.Collections;

public class flipper : MonoBehaviour {

    private float startPosition = 0;
    private float actionPosition = 45;
    private float flipperDamper = 1; 
    public float flipperStregth = 10;

    public System.String inputButtonName = "LeftFlipper";

    // Use this for initialization
    void Awake () {
        
        GetComponent<HingeJoint>().useSpring = true;

	}
	// Update is called once per frame
	void FixedUpdate () {

        JointSpring flipper = new JointSpring();
        HingeJoint hinge = GetComponent<HingeJoint>();
        JointLimits limits = hinge.limits;

        flipper.spring = flipperStregth;
        flipper.damper = flipperDamper;

        if (Input.GetButton(inputButtonName))
            flipper.targetPosition = actionPosition;
        else
            flipper.targetPosition = startPosition;

        hinge.spring = flipper;
        hinge.useLimits = true;
        limits.min = startPosition;
        limits.max = actionPosition;
    }
}
