﻿using UnityEngine;
using System.Collections;

public class Kicker : MonoBehaviour {

	public float force = 1000f;
    public Rigidbody ball;
	// Use this for initialization
	void Start () {
        ball = GameObject.Find("ball").GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter (Collision collision) {
		if (collision.transform.tag == "ball") {
            GameManager.Score = GameManager.Score + 5;
            collision.rigidbody.AddExplosionForce(force+ball.velocity.magnitude,transform.position,10.0f,3.0f);
			}
		}
	}
