﻿using UnityEngine;
using System.Collections;

public class newBall : MonoBehaviour {
    public GameObject limite;
    // Use this for initialization
    void Start () {
        limite = GameObject.Find("limite");
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ball")
        {
            limite.SetActive(false);
            coinButton.lives--;
            other.transform.localPosition = new Vector3(-4.6f, 5.2f, 17.87176f);
        }
    }
}
