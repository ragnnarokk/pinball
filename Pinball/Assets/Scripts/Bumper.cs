﻿using UnityEngine;
using System.Collections;

public class Bumper : PointsGiver {

	public float scaleTo = 1.2f;
	private Transform mesh;
	private bool scale;
	private float scaleRate = 0.05f;

	// Use this for initialization
	void Start () {
		//base.Start();
        mesh = gameObject.transform.GetChild(0);
	}
	
	// Update is called once per frame
	void Update () {
	
		if (scale)
		{
			if (mesh.localScale.x < 1f)
                scaleRate = Mathf.Abs(scaleRate);
			else if (mesh.localScale.x > scaleTo)
                scaleRate = -Mathf.Abs(scaleRate);

            mesh.localScale += Vector3.one * scaleRate;

			if (mesh.localScale.x < 1f)
                scale = false;
		}
	}

	void OnCollisionEnter (Collision collision) {
		if (collision.transform.tag == "ball") {
			//AddPoints();
            scale = true;
		}
	}
}
