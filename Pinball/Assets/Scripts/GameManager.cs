﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {
    //public Light light;
    private Text text;
    static public bool start = false;
    static public int Score = 0;
    static public int triple = 0;
	// Use this for initialization
	void Start () {
        //light.enabled = false;
        text = Canvas.FindObjectOfType<Canvas>().GetComponentInChildren<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        text.text = "Lives " + coinButton.lives +"\r\n"+ "Score " + Score.ToString();
        if (triple >= 4)
        { 
            if (coinButton.lives != 0) {
                Score = Score * 3; }
            triple = 0;
        }
        if (coinButton.lives == 0 && start)
        {
            text.color = Color.white;
            triple = 3;
        }
        else
            text.color = Color.gray;
	}
}
