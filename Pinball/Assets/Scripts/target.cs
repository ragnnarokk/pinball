﻿using UnityEngine;
using System.Collections;

public class target : MonoBehaviour {
    public float force = 120;
    public bool arriba = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (GameManager.triple >= 3 && arriba == false)
        {
            StartCoroutine("up");
            this.GetComponentInChildren<Light>().enabled = true;
            arriba = true;
        }
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ball")
        {
            StartCoroutine("down");
            GameManager.Score = GameManager.Score + 10;
            this.GetComponentInChildren<Light>().enabled = false;
        }
    }
    IEnumerator down()
    {
        for (float force2 = 0;force2 != force; force2 += 10)
        {
            transform.Translate(-10 * Time.deltaTime, 0, 0);
            yield return null;
        }
        arriba = false;
        GameManager.triple++;
    }
    IEnumerator up()
    {
        for (float force2 = 0; force2 != force; force2 += 10)
        {
            transform.Translate(10 * Time.deltaTime, 0, 0);
            yield return null;
        }
        if(GameManager.triple !=0)
        GameManager.triple = 4;
    }

}