﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shooter : MonoBehaviour {

    public float distance = 50;
    public float speed = 1;
    public float power = 1000;
    private bool shot = false;
    private float position = 0;
    private bool fire = false;
    public System.String inputButtonName = "Pull";

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if (Input.GetButton(inputButtonName) && shot == true)
        {
            if(position < distance)
            {
                this.GetComponent<Rigidbody>().constraints = ~RigidbodyConstraints.FreezePositionY;
                transform.Translate(0, -speed * Time.deltaTime, 0);
                position += speed * Time.deltaTime;
                fire = true;
            }
        }
        else if(position > 0)
        {
            if(fire && shot)
            {
                this.transform.TransformDirection(Vector3.up * 20);
                this.GetComponent<Rigidbody>().AddForce(0, position * power,0);
                fire = true;
                shot = true;
                }
            position = position-1*Time.deltaTime;
        
        }
        if(position <= 0)
        {
            fire = false;
            position = 0;
        }

	}

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "ball")
        {
            shot = true;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        this.transform.localPosition = new Vector3(1.5f, 5.8f, 17.9f);
        shot = false;
    }
}
